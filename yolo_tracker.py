from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys
import imagenet_label_list
import time

import cv2
import numpy as np
import tensorflow as tf

import sys
sys.path.insert(0, 'YOLO_tensorflow')

import YOLO_tiny_tf

size = 192

def load_graph(model_file):
    graph = tf.Graph()
    graph_def = tf.GraphDef()

    with open(model_file, "rb") as f:
        graph_def.ParseFromString(f.read())
    with graph.as_default():
        tf.import_graph_def(graph_def)

    return graph

def load_labels(label_file):
    label = []
    proto_as_ascii_lines = tf.gfile.GFile(label_file).readlines()
    for l in proto_as_ascii_lines:
        label.append(l.rstrip())
    return label

if __name__ == "__main__":
    input_height = 299
    input_width = 299

    #For average time
    s = 0

    with tf.Session() as sess:
        yolo = YOLO_tiny_tf.YOLO_TF()
        graph = tf.get_default_graph()
        vc = cv2.VideoCapture()
        start = time.time()
        if (vc.open(0)):
            while True:
                #Read from camera
                ret, frame = vc.read()

                #Prep image
                img = cv2.resize(frame, dsize=(size, size), \
                                 interpolation = cv2.INTER_LINEAR)
                np_img = np.asarray(img)
                np_final = np.expand_dims(np_img, axis=0)
                norm = (np_final - np.mean(np_final)) / np.std(np_final)

                start = time.time()
                #Run neural network
                yolo.detect_from_cvmat(frame)

                #Disp Image
                #cv2.imshow("IM", img)
                #if cv2.waitKey(1) == 27:
                    #break

                # Timing
                delta = time.time() - start
                s = .9 * s + .1 * delta
                print("Delta: " + str(s))
