from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys
import imagenet_label_list
import time

import cv2
import numpy as np
import tensorflow as tf

size = 192

def load_graph(model_file):
    graph = tf.Graph()
    graph_def = tf.GraphDef()

    with open(model_file, "rb") as f:
        graph_def.ParseFromString(f.read())
    with graph.as_default():
        tf.import_graph_def(graph_def)

    return graph

if __name__ == "__main__":
    model_file = "MobileNet/mobilenet_v1_0.25_192.pb"
    input_layer = "input"
    output_layer = "output"

    graph = load_graph(model_file)

    #Print possible tensors for operations
    for op in graph.get_operations():
        print(op.name)

    #Get tensors for operations 
    input_name = "import/" + input_layer
    output_name = "import/" + output_layer

    input_operation = graph.get_operation_by_name(input_name);
    output_operation = graph.get_operation_by_name(output_name);

    #For average time
    s = 0

    with tf.Session(graph=graph) as sess:
        #saver = tf.train.import_meta_graph(meta_graph)
        #saver.restore(sess, tf.train.latest_checkpoint("./"))
        #graph = tf.get_default_graph()
        vc = cv2.VideoCapture()
        start = time.time()
        if (vc.open(0)):
            while True:
                #Read from camera
                ret, frame = vc.read()

                #Prep image
                img = cv2.resize(frame, dsize=(size, size), \
                                 interpolation = cv2.INTER_LINEAR)
                img=cv2.normalize(img.astype('float'), None, -0.5, .5, cv2.NORM_MINMAX)
                np_img = np.asarray(img)
                np_final = np.expand_dims(np_img, axis=0)
                #np_final = (np_final - np.mean(np_final)) / np.std(np_final)

                start = time.time()
                #Run neural network
                results = sess.run(output_operation.outputs[0],
                                  {input_operation.outputs[0]: np_final})
                results = np.squeeze(results)

                # Timing
                delta = time.time() - start
                s = .9 * s + .1 * delta
                print("Delta: " + str(s))

                #Disp Image
                cv2.imshow("IM", img)
                if cv2.waitKey(1) == 27:
                    break

                #Disp Results
                top_k = results.argsort()[-1:][::-1]
                for i in top_k:
                    print(imagenet_label_list.labels[i-1])
                    print(results[i])

