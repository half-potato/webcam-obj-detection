# Process
The checkpoints do not work directly, so the script freeze_mobilenets.py was used to convert the .ckpts to .pb frozen graphs for use in recognition.

The next step was to figure out what the operations were to tell tensorflow which tensors to use when processing stuff. Once the graph is loaded, these are listed in the graph.get_operations(), which can be iterated through.

This operation can then be retrieved by using graph.get_operation_by_name() once you have selected the one you want.

The image being passed in has to be normalized for good results.
